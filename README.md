#WikiTweet

Created with Python, Django and Javascrit.

- REPO: https://gitlab.com/gilsanjuan/wikitweet
- URL:  https://wikitweet.herokuapp.com/


##Acceptance Criteria:

1. Given a user, when they access your application, then they should be presented with a search box prompting them for a topic.
2. Given a user, when they enter a topic, results from Twitter should be returned.
3. Given a user, when they enter a topic, results from Wikipedia should be returned.
4. Given a user who's performed a search, when they hit the browser's refresh button, results should be refreshed under the same search criteria.


##Solution:


###Backend

The application was developed in Django, does not use database, 
only generic class views, form, and third-party packages that 
perform logic APIs are used.


####Third-party packages APIs:

* Twitter: https://github.com/geduldig/TwitterAPI
* Wikipedia: https://github.com/goldsmith/Wikipedia

####Implemantation


**wikiTweetSearch**

The implementation of the APIs are abstracted in the class, this is on *root_project/libs/wikiTweetSearch*. 

Instantiating the class third party packages is initialized on the case of data access twitter is introduced.


*This class contains five methods:*

    twitter_search (self, query, lat = None, lng = None): 
      Searches for tweets, if values lat(latitude), lng(longitude) the search is performed by a geolocation 100
    mile radius, otherwise a simple search is done on the call, returns a list 
    [{'user':'example','text':'text example', 'image': 'http...'}] of each tweet.

    wiki_simple_search (self, query): 
      Make a simple search on wikipedia.
    
    wiki_geosearch (self, query, lat, lng): 
      Performs a geographic search with the maximum radius allowed within 10,000 meters(6.21 miles).

    wiki_search (self, query, lat, lng):
      Is responsible for deciding if the query is simple or geographic, returns a list 
    [{'title':'title example', 'url':'http://example.com'},{...},{...}] of each results returned by the API.

**Django app**

The main application is on django *root_project/apps/web*, uses a form (forms.py) and two views (views.py):

SearchForm contains the fields:

- query (Charfield)
- geo (BooleanField) to display the checkbox
- latitude (floatfield)
- longitude (floatfield)

**Views**

**QueryView(TemplateView):** This view is responsible for capturing the parameters in the URL and call the wikiTweetSearch and add context and the results returned by the APIs.

**HomeView(FormView):** handles QueryView redirect to view based on the parameters that are in the form SearchForm.


###Frontend

**Tools:**

HTML5Boilerplate for the structure, and Bootstrap is used with sass for styles.

- HTML5Boilerplate
- Bootstrap With SASS
- SASS
- Jquery

**Templates (root_project / templates):**

- Base.html: Main structure.
- Home.html: Form, extending base.html.
- Query.html: Show results, extending base.html.

**Assets:**

*root_project/static/assets/css*:

- Main.css

*root_project/static/assets/ico*

- Favicon.ico
- Apple-touch-icon.png

*root_project/static/assets/js:*

- Main.js

*root_project/static/assets/js/vendor*

- Jquery-1.11.2.min.js


**Geolocation:**

A simple script developed used HTML5 APIs for geolocation and local storage.
*root_project/static/assets/js/main.js*


When activated ckecbox:

- Ask user permission to geolocation.
- The script disables the search button to get results.
- Save the result in localStorage.


###Bottlenecks

- Document myself about the required APIs, because I didn't have 
   any knowledge about it, especially twitter api where you need 
   to register an application for access.

- try different packages for apis.

- Script for geolocation, took more time than expected, 
   because do not imagine the possible use cases until 
   you are developing it.

- Many problems in implementing on Heroku, especially with static files.


###Future


- Under another branch I work to convert the views in a small api, and started to work on implementation with angularjs to consume this API, but I lacked time.

- caching the results of the APIs to expedite requests.