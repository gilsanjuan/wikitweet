from django import forms

class SearchForm(forms.Form):
    query = forms.CharField(max_length=100)
    geo = forms.BooleanField(required=False)
    latitude = forms.FloatField(required=False)
    longitude = forms.FloatField(required=False)
