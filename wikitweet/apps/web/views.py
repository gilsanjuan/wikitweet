from django.core.urlresolvers import reverse_lazy
from django.shortcuts import HttpResponseRedirect
from django.utils.text import slugify
from django.views.generic import FormView, TemplateView
from libs.wikiTweetSearch import WikiTweetSearch
from .forms import SearchForm


class HomeView(FormView):
    template_name = 'home.html'
    form_class = SearchForm

    def form_valid(self, form):
        query = slugify(form.cleaned_data['query'])
        lat = form.cleaned_data['latitude']
        lng = form.cleaned_data['longitude']

        if lat and lng:
            return HttpResponseRedirect(reverse_lazy('geo_query', kwargs={'query': query, 'lat': lat, 'lng': lng}))
        else:
            return HttpResponseRedirect(reverse_lazy('query', kwargs={'query': query}))


class QueryView(TemplateView):
    template_name = 'query.html'
    api = WikiTweetSearch()

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        query = kwargs.get('query', '')
        lat = kwargs.get('lat', None)
        lng = kwargs.get('lng', None)

        if query:
            context['query'] = query.replace('-', ' ')
            context['twitter'] = self.api.twitter_search(query, lat, lng)
            context['wikipedia'] = self.api.wiki_search(query, lat, lng)

        return self.render_to_response(context)
