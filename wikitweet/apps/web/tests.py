from django.core.urlresolvers import resolve
from django.test import TestCase
from django.utils.text import slugify
from .forms import SearchForm

class HomeSimpleTest(TestCase):
    def test_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_forms(self):
        form_data = {'query': 'something'}
        form = SearchForm(data=form_data)
        self.assertEqual(form.is_valid(), True)

    def test_forms_many_words(self):
        form_data = {'query': slugify('University of Guadalajara')}
        form = SearchForm(data=form_data)
        self.assertEqual(form.is_valid(), True)


class QuerySimpleTest(TestCase):
    def test_home(self):
        response = self.client.get('/q/uno/')
        self.assertEqual(response.status_code, 200)

    def url_simple_search(self):
        resolver = resolve('/q/uno/')
        self.assertEqual(resolver.url_name, 'query')

    def url_geolocation_search(self):
        resolver = resolve('/q/uno/20.71669960022/103.40000152588/')
        self.assertEqual(resolver.url_name, 'geo_query')
