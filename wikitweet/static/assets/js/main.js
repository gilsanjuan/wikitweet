$(function () {
    var wikiTweet;

    function WikiTweetGeo() {
        this.searchBtn = $('#search');
        this.checkboxInput = $('#id_geo');
        this.latitudeInput = $('#id_latitude');
        this.longitudeInput = $('#id_longitude');
        this.checkboxWrapper = this.checkboxInput.closest('div');
        this.checkboxCheked = JSON.parse(localStorage.getItem('geo_check')) || false;
        this.coordinates = JSON.parse(localStorage.getItem('geo_coordinates')) || {};
    }

    // check and uncheck
    WikiTweetGeo.prototype.checkboxInputCheked = function () {
        this.checkboxInput.prop('checked', true);
    };
    WikiTweetGeo.prototype.checkboxInputUncheked = function () {
        this.checkboxInput.prop('checked', false);
    };

    // Setter.
    WikiTweetGeo.prototype.setCheckboxCheked = function (bool) {
        this.checkboxCheked = bool;
        localStorage.setItem('geo_check', bool);
    };
    WikiTweetGeo.prototype.setCoordinates = function (latLng) {
        this.coordinates = latLng;
        localStorage.setItem('geo_coordinates', JSON.stringify(latLng));
    };

    // populate and depopulate inputs
    WikiTweetGeo.prototype.populateInputs = function () {
        this.latitudeInput.val(this.coordinates.lat);
        this.longitudeInput.val(this.coordinates.lng);
    };
    WikiTweetGeo.prototype.depopulateInputs = function () {
        this.latitudeInput.val('');
        this.longitudeInput.val('');
    };

    // Errors
    WikiTweetGeo.prototype.showError = function(msg) {
        var html = '<div class="alert alert-danger text-warning" role="alert">' + msg +'</div>';
        $(html).insertAfter(this.checkboxWrapper).delay(5000).fadeOut(500, function(){ this.remove()});
	};

    // show load icon and disabled submit button.
    WikiTweetGeo.prototype.loading = function(){
        var html = '<span class="glyphicon glyphicon-refresh glyphicon-load-animate"></span>';
        this.checkboxWrapper.append(html);
        this.searchBtn.prop( "disabled", true );
    };
    // remove load icon and enabled submit button.
    WikiTweetGeo.prototype.loadingDone = function(){
        $('.glyphicon-load-animate').fadeOut(400, function(){ this.remove()})
        this.searchBtn.prop( "disabled", false );
    };

    function GeoLocation() {
        var self = this;

        this.loading();
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError);

        function geoSuccess(position) {
            var geo = {lat: position.coords.latitude, lng: position.coords.longitude};
            self.setCoordinates(geo);
            self.populateInputs();
            self.loadingDone();
        }

        function geoError(error) {
            var errors = {
                1: 'Authorization fails', // permission denied
                2: 'Can\'t detect your location', //position unavailable
                3: 'Connection timeout' // timeout
            };
            self.showError('Error: ' + errors[error.code]);
            self.setCheckboxCheked(false);
            self.checkboxInputUncheked();
            self.loadingDone();
        }
    }

    function init() {
        wikiTweet = new WikiTweetGeo();

        if (navigator.geolocation) {
            if (wikiTweet.checkboxCheked) {
                wikiTweet.checkboxInputCheked();

                if ($.isEmptyObject(wikiTweet.coordinates)) {
                    GeoLocation.call(wikiTweet);
                } else {
                    wikiTweet.populateInputs();
                }
            }
        } else {
            wikiTweet.checkboxWrapper.hide();
        }

        initEvent();
    }

    function initEvent() {
        wikiTweet.checkboxInput.on("click", function () {
            if ($(this).is(':checked')) {
                wikiTweet.setCheckboxCheked(true);

                if ($.isEmptyObject(wikiTweet.coordinates)) {
                    GeoLocation.call(wikiTweet);
                } else {
                    wikiTweet.populateInputs();
                }
            } else {
                wikiTweet.setCheckboxCheked(false);
                wikiTweet.depopulateInputs();
            }
        });
    }

    init();
});
