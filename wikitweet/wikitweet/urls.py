"""wikitweet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, patterns, url
from apps.web.views import HomeView, QueryView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^q/(?P<query>[\w-]+)/$', QueryView.as_view(), name='query'),
    url(r'^q/(?P<query>[\w-]+)/(?P<lat>(-?\d+\.\d+))/(?P<lng>(-?\d+\.\d+))/$',
        QueryView.as_view(), name='geo_query'),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
