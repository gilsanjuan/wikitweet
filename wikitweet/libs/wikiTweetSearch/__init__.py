import wikipedia
from django.conf import settings
from TwitterAPI import TwitterAPI
from libs.Singleton import singleton


@singleton
class WikiTweetSearch(object):
    """
        TwitterApi and wikipedia was Initialize on variables in __init__.

        Methods:
            twitter_search: parameters query(text) and lat, lng are optional, return a dict.
            wikipedia_search: parameters query(text) and lat, lng are optional, return a dict.
    """

    def __init__(self):
        self.wiki = wikipedia
        self.twitter = TwitterAPI(settings.CONSUMER_KEY, settings.CONSUMER_SECRET,
                                  settings.ACCESS_TOKEN_KEY, settings.ACCESS_TOKEN_SECRET)

    def twitter_search(self, query, lat=None, lng=None):
        results = []
        if query:
            query_dict = {'q': query, 'language': 'en'}

            if lat and lng:
                query_dict['geocode'] = '%s,%s,%s' % (lat, lng, '100mi')

            request = self.twitter.request('search/tweets', query_dict)

            for item in request.get_iterator():
                results.append({
                    'user': item['user']['screen_name'], 'text': item['text'],
                    'image': item['user']['profile_image_url_https']})

        return sorted(results, key=lambda result: result['user'])

    def wiki_search(self, query, lat, lng):
        results = []
        self.wiki.set_lang("en")

        if query:
            if lat and lng:
                request = self.wiki_geosearch(query, lat, lng)
            else:
                request = self.wiki_simple_search(query)

            for item in request:
                results.append({'title': item, 'url': self.wiki_url_parser(item)})

        return sorted(results)

    def wiki_simple_search(self, query):
        return self.wiki.search(query)

    def wiki_geosearch(self, query, lat, lng):
        return self.wiki.geosearch(latitude=lat, longitude=lng, title=query, radius=10000)

    def wiki_url_parser(self, title):
        """
        title_url: transform unicode to str, and replace ' ' to '_', finally the first two characters and the
                    last are removed with [2:-1] to form wikipedia url.

                    for example: u"S\u014dsuke Uno" => "u'S\xc5\x8dsuke_Uno'" => 'S\xc5\x8dsuke_Uno'
        """
        wiki_baseurl = 'https://en.wikipedia.org/wiki/'
        title_url = str(repr(title).decode('unicode-escape').encode('UTF-8')).replace(' ', '_')[2:-1]
        wiki_url = '%s%s' % (wiki_baseurl, title_url)

        return wiki_url

